#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include "g.h"
int State = 0;
datanode T;
int readline(char *arr, int len) {
	int i = 0;
	int ch;
	while(((ch = getchar()) != '\n') && (i < len - 1)) {
		arr[i] = ch;
		i++;
	}
	arr[i] = '\0';
	return i;
}
int main() {
	char str[MAX];
	int x = 0; 
	Integer *ans;
	initdata(T);
	while(x = readline(str, MAX)) {
		if(strcmp(str, "quit") == 0)
			return 0;
		ans = infix(str);
		if(State == 6) {
			State = 0;
			continue;
		}
		if(State == 8) {
			State = 0;
			printf("syntax error\n");
			continue;
		}
		State = 0; 
		PrintInteger(ans);
		free(ans);
	}
	return 0;
}
